var home = require('../app/controllers/home');

//you can include all your controllers

module.exports = function (app, passport) {

    app.get('/login', home.login);
    app.get('/signup', home.signup);

    app.get('/', home.loggedIn, home.home);//home
    app.get('/home', home.loggedIn, home.home);//home

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/home', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/home', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));


}
function get_data() {
    var flag = false;
    var ourRequest = new XMLHttpRequest();
    ourRequest.open("GET", "jsonResult.json");
    var data = {
        email: document.getElementById("email").value,
        password: document.getElementById("password").value,
        repeatedpassword: document.getElementById("repeatedpassword").value,
        fullname: document.getElementById("fullname").value,
        address: document.getElementById("address").value
    };
    console.log(data);
    var json_send = JSON.stringify(data);

    ourRequest.onreadystatechange = function () {
        if (ourRequest.readyState === 4 && ourRequest.status === 200) {
            var json_string = JSON.parse(ourRequest.responseText);

            if (json_string.result === 0) {

                var msg_email = document.getElementById("msg_email");
                if (msg_email !== null)
                    msg_email.innerHTML = json_string.error.msg_email;
                
                var msg_password = document.getElementById("msg_password");
                if (msg_password !== null)
                    msg_password.innerHTML = json_string.error.msg_password;
                
                var msg_repeatedpassword = document.getElementById("msg_repeatedpassword");
                if (msg_repeatedpassword !== null)
                    msg_repeatedpassword.innerHTML = json_string.error.msg_repeatedpassword;

            } else {
                flag = true;
                var t = confirm("Đăng kí thành công");
                if (t === true)
                    location = "https://www.facebook.com/";
            }
        }
    };
    ourRequest.send(json_send);
    return flag;
}
